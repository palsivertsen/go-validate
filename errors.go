package validate

import "fmt"

type RuleError struct {
	RuleName     string
	wrappedError error
}

func NewRuleError(ruleName, message string) RuleError {
	return RuleError{
		RuleName:     ruleName,
		wrappedError: messageError(message),
	}
}

func WrapRuleError(ruleName string, err error) RuleError {
	return RuleError{
		RuleName:     ruleName,
		wrappedError: err,
	}
}

func (e RuleError) Error() string {
	if e.wrappedError != nil {
		return fmt.Sprintf("rule %s: %s", e.RuleName, e.wrappedError)
	}

	return "rule %s" + e.RuleName
}

func (e RuleError) Unwrap() error {
	return e.wrappedError
}

type messageError string

func (e messageError) Error() string {
	return string(e)
}
