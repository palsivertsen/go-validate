package validate_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"validate"
)

func TestWrapRuleError(t *testing.T) {
	t.Parallel()

	testError := TestError("test error")

	unit := validate.WrapRuleError("TestRuleName", testError)

	var wrappedErr TestError
	require.ErrorAs(t, unit, &wrappedErr)
	require.ErrorIs(t, wrappedErr, testError)
	require.Equal(t, testError, wrappedErr)
	require.ErrorIs(t, unit, testError)
}

func TestNewRuleError(t *testing.T) {
	t.Parallel()

	unitA := validate.NewRuleError("TestRuleName", "test message")
	unitB := validate.NewRuleError("TestRuleName", "test message")
	unitC := validate.NewRuleError("TestRuleName", "another message")

	require.ErrorIs(t, unitA, unitB)
	require.NotErrorIs(t, unitA, unitC)
}

// helpers

type TestError string

func (e TestError) Error() string {
	return string(e)
}
