package validate

type (
	Rule[T any]  func(t T) error
	Rules[T any] []Rule[T]
)

func (r Rules[T]) Validate(t T) error {
	for _, rule := range r {
		if err := rule(t); err != nil {
			return err
		}
	}
	return nil
}
