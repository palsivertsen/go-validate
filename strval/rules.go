package strval

import (
	"validate"
)

func Length(l int) validate.Rule[string] {
	return func(str string) error {
		if len(str) == l {
			return nil
		}
		return validate.RuleError{
			RuleName: "Length",
		}
	}
}

func NotLength(l int) validate.Rule[string] {
	return func(str string) error {
		if len(str) != l {
			return nil
		}
		return validate.RuleError{
			RuleName: "NotLength",
		}
	}
}
