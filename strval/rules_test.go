package strval_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"validate"
	"validate/strval"
)

func TestRules_Validate(t *testing.T) {
	t.Parallel()
	// define tests
	tests := []struct {
		testName        string
		ruleName        string
		ruleFunc        validate.Rule[string]
		inverseRuleName string
		inverseRuleFunc validate.Rule[string]
		oks             []string
		failures        []string
	}{
		{
			testName:        "Length 5",
			ruleName:        "Length",
			ruleFunc:        strval.Length(5),
			inverseRuleName: "NotLength",
			inverseRuleFunc: strval.NotLength(5),
			oks:             []string{"12345", "aaaaa", "     "},
			failures:        []string{"1234", "", " "},
		},
	}
	// define helper functions
	testOKs := func(ruleFunc validate.Rule[string], oks []string) func(t *testing.T) {
		t.Helper()
		return func(t *testing.T) { //nolint:thelper
			for _, ok := range oks {
				err := ruleFunc(ok)
				require.NoError(t, err)
			}
		}
	}
	testFailures := func(ruleFunc validate.Rule[string], ruleName string, failures []string) func(t *testing.T) {
		t.Helper()
		return func(t *testing.T) { //nolint:thelper
			for _, failure := range failures {
				var err validate.RuleError
				require.ErrorAs(t, ruleFunc(failure), &err, "error should be of type validate.RuleError")
				assert.Equal(t, ruleName, err.RuleName, "rule name in error")
			}
		}
	}

	// run tests
	for _, tt := range tests {
		t.Run(tt.testName, func(t *testing.T) {
			t.Run("ok", testOKs(tt.ruleFunc, tt.oks))
			t.Run("failures", testFailures(tt.ruleFunc, tt.ruleName, tt.failures))

			// inverse version of rule
			t.Run("inverse ok", testOKs(tt.inverseRuleFunc, tt.failures))
			t.Run("inverse failures", testFailures(tt.inverseRuleFunc, tt.inverseRuleName, tt.oks))
		})
	}
}
